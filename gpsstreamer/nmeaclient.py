#!/usr/bin/python
import socket
import sys
import threading
import time
import argparse


class NMEAClient(threading.Thread):
    def __init__(self, address="localhost", port=2948):
        """
        A basic client to receive data from the nmea server
        :param address: Address of server, localhost default
        :type address: str
        :param port: port to received data from, 2948 default
        :type port: int
        """
        super(NMEAClient, self).__init__()
        self.daemon = True
        self._stopper = threading.Event()
        self.address = address
        self.port = port
        self.sock = None
        self.data = []
        self._reading = False
        
        # Create a TCP/IP socket
        try:
            if not self.wait_for_sock():
                self.sock = None
        except KeyboardInterrupt:
            raise
    
    def stop(self):
        """
        Requests a stop of the client thread
        :return: None
        """
        self._stopper.set()
    
    def wait_for_sock(self):
        """
        Wait for socket to connect to server
        :return: None
        """
        # close existing socket if recalled
        if self.sock is not None:
            self.sock.close()
        # get new socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        # Connect the socket to the port where the server is listening
        server_address = (self.address, self.port)
        print('connecting to %s port %s' % server_address)
        while True:
            try:
                self.sock.connect(server_address)
                return True
            except socket.error:
                time.sleep(1)
            except KeyboardInterrupt:
                raise
    
    def isstopped(self):
        """
        Checks if the stop request has been made
        :return: bool
        """
        return self._stopper.is_set()
    
    def run(self, bytes_to_rec=1024, line_buf_max=1024):
        """
        Runs the client receiving specified bytes
        each string or part string received is stored in an array, doesn't handle part strings when emptying buffer
        therefor data may get corrupted if not read often, alternatively set the buffer very high to account longer
        read intervals.
        :param line_buf_max: amount of nmea strings to store as maximum, once reached some strings may be corrupted.
        :type line_buf_max: int
        :param bytes_to_rec: Amount of bytes to receive in each request
        :type bytes_to_rec: int
        :return: 0 when completed
        """
        while True:
            try:
                time_out = 0
                while not self.isstopped():
                    if not self._reading:
                        if self.sock is not None:
                            data = self.sock.recv(bytes_to_rec).splitlines(True)
                            if data:
                                for line in data:
                                    self.data.append(line)
                                time_out = 0
                            else:
                                if time_out >= 5:
                                    # time_out = 0
                                    raise socket.error("Socket Timeout")
                                else:
                                    time_out += 1
                                    time.sleep(1)
                            # remove data if too long
                            if len(self.data) >= line_buf_max:
                                self.data = self.data[len(data) + 1:]
                        
                        else:
                            time.sleep(1)
            except socket.error as e:
                print("Caught socket exception {0}".format(e))
                if not self.wait_for_sock():
                    return 1
            except KeyboardInterrupt:
                raise
            
            if self.isstopped():
                return 0
    
    def close(self):
        """
        Closes the socket connection
        :return: None
        """
        self.stop()
        while self.is_alive():
            time.sleep(0.1)
        self.sock.close()
    
    def read(self, bytes_to_read=64):
        """
        Reads a set amount of bytes from the data buffer
        :param bytes_to_read: Amount of bytes to read or -1 to read all available
        :return: Data received or None if 0 not enough bytes in buffer
        """
        self._reading = True
        rdata = None
        if bytes_to_read == -1:
            bytes_to_read = len(self.data)
        
        if len(self.data) >= bytes_to_read:
            rdata = self.data[:bytes_to_read]
            self.data = self.data[bytes_to_read:]
        self._reading = False
        return rdata


def main(args):
    """
    Main client run routine, ability to record data to file and or output to screen by default
    :param args: sys args, parsed with argparse
    :return: sys exit codes
    """
    parser = argparse.ArgumentParser(description='Connects to A NMEA server and prints the data')
    parser.add_argument('-a', '--address', default='localhost', type=str, help='IP address of NMEA streamer')
    parser.add_argument('-p', '--port', default=2947, type=int, help='port NMEA Streamer')
    parser.add_argument('-l', '--log', default=False, type=argparse.FileType('wb'), help='log output to file')
    parser.add_argument('-o', '--out', default=False, action='store_true',
                        help='send output to screen, default off if file selected, else on')
    parser.add_argument('-d', '--data', default=-1, type=int,
                        help='amount of bytes to read per cycle default all available')
    args = parser.parse_args(args)
    if not args.log:
        args.out = True
    
    client = NMEAClient(args.address, args.port)
    client.start()
    try:
        while client.is_alive():
            data = client.read(-1)
            if data is not None:
                j_data = b''.join(data).decode('utf-8')
                if args.log:
                    args.log.write(j_data)
                if args.out:
                    sys.stdout.write(j_data)
    except KeyboardInterrupt:
        print("\nClosing Client...please wait\n")
        client.close()
    
    # wait for sockets to close properly
    force_close_count = 0
    while client.is_alive():
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            if force_close_count >= 5:
                print("Force Closing!")
                break
            else:
                force_close_count += 1
                print("Please wait while closing...")


if __name__ == "__main__":
    main(sys.argv[1:])
