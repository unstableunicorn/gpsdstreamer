from setuptools import setup

setup(
    name='gpsdstreamer',
    version='1.0',
    packages=[''],
    url='https://gitlab.com/elrichindy/gpsdstreamer',
    license='MIT',
    author='elric',
    author_email='elrichindy@gmail.com',
    description='Stream a Nmea GPS file connect to GPSD instance and stream json or nmea gps data over a netork',
    entry_points={'console_scripts': ['gpsserver=nmeaserver:main']}
)
