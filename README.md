# GPS Streaming over TCP
The gpsdstreamer will stream valide nmea (or json if specified) gps stream over tcp
## Quick Start
To get up and running with the default streaming of a loop around canberra ariport
use docker-compose and run 
```
docker-compose up -d
```
This will stream to the default port od 2948
## Usage
You can use this image to stream any valid nmea or json file over tcp
To get help you can run the images with --help option as below after building:
```
docker build -t gpsstreamer .
docker run -it --rm gpsstreamer --help
```
This will print:
```
usage: gpsserver [-h] [-a <ip address>] [-p <port number>] [-j]
                 [-i <path/to/file>] [-r] [-n] [-o]

gpsd server wrapper to stream gps data from gpsd or file

optional arguments:
    -h, --help            show this help message and exit
    -a <ip address>, --address <ip address>
                          IP address of NMEA streamer (default: 0.0.0.0)
    -p <port number>, --port <port number>
                          port to send data to (default: 2948)
    -j, --json            output or input is json/dict gpsd default data instead
                          of nmea (default: False)
    -i <path/to/file>, --input <path/to/file>
                          send recorded gps data from a file (default: False)
    -r, --repeat          repeat file data until ctrl -c otherwise just run once
                          (default: False)
    -n, --newtime         used with fake gps data to replace old time and date
                          with current utc times and dates (default: False)
    -o, --printout        print output to screen (default: False)
```

There are several testdata streams that can be used shown in the testdata folder.
See [testdata](https://gitlab.com/elrichindy/gpsdstreamer/tree/master/gpsstreamer/testdata) on gitlab for details

### Using local files
to use a local file for streaming mount the file or folder.
i.e. if you have a folder in home call gpsstreams with a file called teststream.nmea:
assuming you have built the image with the name 'gpsstreamer'
```
docker run -d -p 2948:2948 -v ~/gpsstreams/:/gpsstreams gpsstreamer -i /gpsstreams/teststream.nmea -r -n
```
This will mount the folder in your home to the root folder and run the file on repeat (-r) and make sure the times are the latest with the newtime flag (-n)
